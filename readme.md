# Models
## Firing a JupyterLab model
First, make sure that [**Jupyter**](https://jupyter.org/install) is installed on your computer. 
Then, to fire a *Jupyter Lab*, run the following command line:
```bash
jupyter-lab [--ip=IP_ADDRESS] [--port=PORT_NUMBER]
```

Your web browser will then automatically open the *Jupyter Lab* instance. You are then ready to go!
